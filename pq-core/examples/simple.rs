use std::thread::sleep;
use std::time::Duration;

extern crate pq_core;

fn main() {
    let mut game = pq_core::Game::new(1);
    println!("Seed: {}, {}", game.seed, game.character);

    loop {
        game.print_queue();
        game.tick();
        sleep(Duration::from_millis(50));
    }
}
