use std::fmt;

use super::config;

pub const STATS: [&str; 6] = ["STR", "CON", "DEX", "INT", "WIS", "CHA"];

#[derive(Debug)]
pub struct Character {
    pub name: String,
    pub race: String,
    pub class: String,

    pub level: usize,

    pub stats: [usize; 6],

    pub hp_max: usize,
    pub mp_max: usize,
}

impl Character {
    pub fn reroll_name(&mut self) {
        self.name = config::random_name();
    }

    pub fn empty() -> Character {
        Character {
            name: "".to_string(),
            race: "".to_string(),
            class: "".to_string(),
            level: 1,
            stats: [0usize; 6],
            hp_max: 0,
            mp_max: 0,
        }
    }

    pub fn total(&self) -> usize {
        self.stats.iter().sum()
    }
}

impl fmt::Display for Character {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        writeln!(
            f,
            "{}
Race: {}
Class: {}
Level {}
STR: {}
CON: {}
DEX: {}
INT: {}
WIS: {}
CHA: {}
HP: {}
MP: {}",
            self.name,
            self.race,
            self.class,
            self.level,
            self.stats[0],
            self.stats[1],
            self.stats[2],
            self.stats[3],
            self.stats[4],
            self.stats[5],
            self.hp_max,
            self.mp_max
        )
    }
}
