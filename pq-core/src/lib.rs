use std::{time::Instant, collections::VecDeque};

use rand::{thread_rng, Rng, SeedableRng};
use rand_chacha::ChaCha8Rng;

mod task;
use task::Task;

mod progress;
use progress::Progress;

pub mod character;
pub use character::Character;

pub mod config;

const TIME: usize = 1000;

#[derive(Debug, PartialEq, Eq)]
pub enum GameState {
    InterPlot,
    Killing,
    Selling,
    Buying,
}

pub struct Game {
    pub character: Character,
    pub last_tick: Instant,
    pub queue: VecDeque<task::Task>,
    pub inventory: Vec<(String, usize)>,
    pub state: GameState,
    pub taskbar: Progress,
    pub rng: ChaCha8Rng,
    pub seed: u64,
}

impl Game {
    pub fn new(mut s: u64) -> Game {
        if s == 0 {
            s = thread_rng().gen();
        }
        let rng = Game::new_rng(s);

        let mut new = Game {
            character: Character::empty(),
            last_tick: Instant::now(),
            state: GameState::InterPlot,
            queue: VecDeque::new(),
            inventory: Vec::new(),
            taskbar: Progress::new(12),
            rng,
            seed: s,
        };

        new.random_character();

        new.push_task(
            10,
            "Experiencing an enigmatic and foreboding night vision".to_string(),
            "".to_string(),
        );

        new.push_task(
            6,
            "Much is revealed about that wise old bastard you'd underestimated".to_string(),
            "".to_string(),
        );
        new.push_task(
            6,
            "A shocking series of events leaves you alone and bewildered, but resolute".to_string(),
            "".to_string(),
        );
        new.push_task(
            4, 
            "Drawing upon an unrealized reserve of determination, you set out on a long and dangerous journey".to_string(),
            "".to_string(),
        );
        new.push_task(2, "Loading".to_string(), "".to_string());
        new
    }

    pub fn new_rng(s: u64) -> ChaCha8Rng {
        ChaCha8Rng::seed_from_u64(s)
    }

    pub fn random_character(&mut self) {
        self.character = Character {
            name: config::random_name(),
            race: config::pick(&config::RACES),
            class: config::pick(&config::KLASSES),
            level: 1,

            stats: self.roll_stats(),

            hp_max: 10,
            mp_max: 10,
        }
    }

    pub fn reroll_stats(&mut self) {
        self.seed = thread_rng().gen();
        self.rng = Game::new_rng(self.seed);
        self.character.stats = self.roll_stats();
    }

    pub fn reset_taskbar(&mut self) {
        self.taskbar.reset(self.queue[0].duration, None);
    }

    pub fn roll_stats(&mut self) -> [usize; 6] {
        let mut empty = [0usize; 6];

        for ele in empty.iter_mut() {
            *ele = 3
                + self.rng.gen_range(0..=6)
                + self.rng.gen_range(0..=6)
                + self.rng.gen_range(0..=6);
        }

        empty
    }

    fn push_task(&mut self, duration: usize, description: String, award: String) {
        self.queue
            .push_back(Task::new(duration * TIME, description, award));

        // if the queue was empty before we added the task, update the taskbar
        // to the task we just added
        if self.queue.len() == 1 {
            self.reset_taskbar();
        }
    }

    fn show(&self) {
        println!("taskbar percentage: {}", self.taskbar.percentage() as usize);
        println!("current task: {}", self.queue[0].get_description());
    }

    fn monster_task(&mut self) {
        let monster = config::pick(&config::MONSTERS);

        let mut iter = monster.split("|");
        let name = iter.next().expect("Expected monster name");

        let mut description = "Killing a ".to_owned();
        description.push_str(name);

        let level = iter
            .next()
            .expect("Expected monster level")
            .parse::<usize>()
            .expect("Failed to parse level");

        let drop = iter.next().expect("Expected monster drop");
        self.push_task(level, description, drop.to_string());
    }

    pub fn tick(&mut self) {
        let mut elapsed = self.last_tick.elapsed().as_millis() as usize;
        if elapsed > 100 {
            elapsed = 100;
        }

        self.taskbar.increment(elapsed);
        if self.taskbar.done() {
            let _ = self.queue.pop_front();

            if self.queue.is_empty() && self.state == GameState::InterPlot {
                self.state = GameState::Killing;
            }

            if self.state == GameState::Killing {
                self.monster_task();
            }

            self.reset_taskbar();
        }

        self.last_tick = Instant::now();
        self.show();
    }

    pub fn print_queue(&self) {
        for t in self.queue.iter() {
            println!("{:?}", t);
        }
    }
}
