#[derive(Debug)]
pub struct Progress {
    position: usize,
    max: usize,
}

impl Progress {
    pub fn new(max: usize) -> Progress {
        Progress { position: 1, max }
    }

    pub fn reset(&mut self, newmax: usize, newposition: Option<usize>) {
        self.max = newmax;
        if let Some(np) = newposition {
            self.position = np;
        } else {
            self.position = 1;
        }
    }

    pub fn increment(&mut self, a: usize) {
        self.position += a;
    }

    pub fn percentage(&self) -> f64 {
        (self.position as f64 / self.max as f64) * 100.0
    }

    pub fn done(&self) -> bool {
        self.position >= self.max
    }
}
