#[derive(Debug)]
pub struct Task {
    //target: String,
    pub duration: usize,
    pub description: String,
    pub award: String,
}

impl Task {
    pub fn new(duration: usize, description: String, award: String) -> Task {
        Task {
            duration,
            description,
            award,
        }
    }

    pub fn get_duration(&self) -> usize {
        self.duration
    }

    pub fn get_description(&self) -> String {
        self.description.clone()
    }

    pub fn get_award(&self) -> String {
        self.description.clone()
    }
}
