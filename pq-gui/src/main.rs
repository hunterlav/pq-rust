use eframe::{egui, epi};
use pq_core::{config, Game};

#[derive(Debug)]
enum WindowState {
    MainMenu,
    NewChar,
    Game,
}

struct RustQuest {
    state: WindowState,
    game: Game,

    roll_queue: Vec<[usize; 6]>,
}

impl epi::App for RustQuest {
    fn name(&self) -> &str {
        "Rust Quest"
    }

    fn update(&mut self, ctx: &egui::CtxRef, frame: &epi::Frame) {
        match &self.state {
            WindowState::MainMenu => self.main_menu(ctx, frame),
            WindowState::NewChar => self.character_create(ctx, frame),
            WindowState::Game => self.game(ctx, frame),
        }
    }
}

impl RustQuest {
    fn new() -> RustQuest {
        let game = Game::new(0);
        let mut roll_queue = Vec::new();
        roll_queue.push(game.character.stats);

        RustQuest {
            state: WindowState::NewChar,
            game,

            roll_queue,
        }
    }

    fn main_menu(&mut self, _ctx: &egui::CtxRef, _frame: &epi::Frame) {
        todo!();
    }

    fn character_create(&mut self, ctx: &egui::CtxRef, _frame: &epi::Frame) {
        egui::TopBottomPanel::bottom("test").show(ctx, |ui| {
            if ui.button("Sold!").clicked() {
                self.state = WindowState::Game;
            }
        });

        egui::CentralPanel::default().show(ctx, |ui| {
            ui.horizontal(|ui| {
                ui.label("Name:");
                ui.text_edit_singleline(&mut self.game.character.name);
                if ui.button("?").clicked() {
                    self.game.character.reroll_name();
                }
            });
            ui.horizontal(|ui| {
                ui.with_layout(egui::Layout::top_down(egui::Align::Min), |ui| {
                    ui.group(|ui| {
                        for k in config::RACES.iter() {
                            ui.radio_value(
                                &mut self.game.character.race,
                                k.to_string(),
                                k.split('|').next().unwrap(),
                            );
                        }
                    })
                });
                ui.with_layout(egui::Layout::top_down(egui::Align::Min), |ui| {
                    ui.group(|ui| {
                        for k in config::KLASSES.iter() {
                            ui.radio_value(
                                &mut self.game.character.class,
                                k.to_string(),
                                k.split('|').next().unwrap(),
                            );
                        }
                    })
                });
                ui.with_layout(egui::Layout::top_down(egui::Align::Min), |ui| {
                    ui.group(|ui| {
                        ui.label("Stats");
                        let mut total: usize = 0;
                        let length = self.roll_queue.len() - 1;
                        egui::Grid::new("stat_grid").spacing(egui::Vec2 {x: 5.0, y: 20.0}).striped(true).show(ui, |ui| {
                            for i in 0..6 {
                                total += self.roll_queue[length][i];
                                ui.label(pq_core::character::STATS[i]);
                                ui.add_enabled_ui(false, |ui| {
                                    ui.text_edit_singleline(
                                        &mut self.roll_queue[length][i].to_string(),
                                    );
                                });
                                ui.end_row();
                            }
                        });
                        ui.add_space(35.0);
                        ui.horizontal(|ui| {
                            ui.label("Total");
                            ui.add_enabled_ui(false, |ui| {
                                ui.add_sized([0.0, 5.0], egui::TextEdit::singleline(&mut total.to_string()));
                                //ui.text_edit_singleline(&mut total.to_string());
                            });
                        });
                    });
                    ui.group(|ui| {
                        if ui.add_sized([100.0, 25.0], egui::Button::new("Roll")).clicked() {
                            self.game.reroll_stats();
                            self.roll_queue.push(self.game.character.stats);
                        }
                        ui.add_enabled_ui(!(self.roll_queue.len() <= 1), |ui| {
                            if ui.add_sized([100.0, 25.0], egui::Button::new("Unroll")).clicked() {
                                self.roll_queue.pop().expect("Queue empty");
                                self.game.character.stats =
                                    self.roll_queue.last().expect("Queue empty").to_owned();
                            }
                        });
                    });
                });
            });
        });
    }

    fn game(&mut self, _ctx: &egui::CtxRef, _frame: &epi::Frame) {
        println!("{}", self.game.character);
        todo!();
    }
}

fn main() {
    let app = RustQuest::new();
    let native_options = eframe::NativeOptions::default();
    eframe::run_native(Box::new(app), native_options);
}
